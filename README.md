# market_6_selenium

Selinium SIDE runner for the Market 6 reports... to BigQuery... to SuperSet

## Background

The customer I was working with sells wholesale to Kroger.  Kroger has a self-service reporting platform called Market 6.  Through this interface you can download reports on how different product categories are doing:  Total Sales, Total Units, Vendor Name, Sales Store, Sales Address, Sales Market, etc..

The customer was often pulling this data manually and downloading as excel and then pulling in other sources to track sales over time.  This was done because there were pieces of information missing from the reports, it took too long to pull history and there were calculated fields they were creating manually to track sales trends.

The Market 6 platform did have an API to pull data from, but this service was an additional cost, which was prohibitive for the customer.  The solution was to create a stored report on the Market 6 platform and then write a selenium job to run the report and then download the report.  The selenium job would run within a custom docker image that also had a samba service installed.  Once the the selenium job was finished, a python script would then pull down the downloaded file using samba and then run through a series of scripts to cleanse and then load the data to BigQuery.

The data was then was used in a report created in SuperSet.

This process was scheduled using Gitlab CI/CD scheduler

![Gitlab Pipeline](images/mk6GitlabPipeline.jpg)

##  Downloading data from Market 6

This will attempt to login to Market6 and download the Matrix reports

This will do this by running a selenium script that was recorded using the [Selenium IDE](https://www.seleniumhq.org/projects/ide/)

The output of the Selenium IDE is called a `.side` file that can be run using the [Selenium Side runner](https://www.npmjs.com/package/selenium-side-runner)
(**Updated on 03/2021**:  The selenium IDE now can export as a python script and this process no longer uses the selenium side runner, but executes through python.  This has allowed us to streamline the process even more and pull out ids and password from the script and run as environment variables)

To do this the service we're running has both a [selenium server](https://www.seleniumhq.org/download/) running and the samba service

This should is run once a week

## Syncing with Big Query.

First we'll load some data to Big Query manually  (first time only, about a year worth of data).  Next we'll setup a script to load the data weekly.

1. Download the new reports from Market 6.
2. Develop a daily key using date and other variables, we'll assume that the latest data from Market 6 takes precedence over old
  1. The data must represent a unique row.
  2. Then merge data from Big Query with the new Data... making sure not to duplicate


![mk6BigQuery.jpg](images/mk6BigQuery.jpg)

## Pulling into SuperSet

We'll then use the final Big Query table and create a connection to view within SuperSet

![images/mk6SuperSet.jpg](images/mk6SuperSet.jpg)

## In this project is a set of notebooks we'll be using to POC everything

Notebooks

## Docker Run commands

Run Docker with jupyter/minimal-notebook (with gcloud installed) that includes gcloud (assumes you are in the directory where your git projects are stored):

```
sudo docker run -p 8888:8888 -v /home/jkbowle/git_projects/market_6_selenium:/home/jovyan/work -it registry.gitlab.com/analyticssupply_engineering/docker-images/python_gcloud_jupyter:3-6-6
```
