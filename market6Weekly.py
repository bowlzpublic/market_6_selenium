'''
This is the script that will run weekly.

This expects the following to be set via environment variables

MK6_PROJECT_ID:  The project id for the App Engine projec
MK6_DATASET_ID:  The dataset where all of the tables will live (this is like "database schema")
MK6_BASE_WORKING_TBLID:  The base working table id
MK6_WEEKLY_INITIAL_TBLID: The ingestion table for the weekly update
MK6_WEEKLY_WORKING_TBLID: The Working table where we pull together the needed information and setup some columns
MK6_REPORTING_TBLID:  This is the final table where all reporting will take place
MK6_WEEKLY_FILENAME:  This is the filename for the Market 6 report that has 4 weeks of data and is run weekly

GOOGLE_APPLICATION_CREDENTIALS:  This is the location of the json file that has the google authentication information
'''
import os
from google.cloud import bigquery

from market6 import utils

dataset_id = os.environ.get('MK6_DATASET_ID')
project_id= os.environ.get('MK6_PROJECT_ID')
working_id = os.environ.get('MK6_BASE_WORKING_TBLID')
weeklyWorking_id = os.environ.get('MK6_WEEKLY_WORKING_TBLID')
weeklyInitial_id = os.environ.get('MK6_WEEKLY_INITIAL_TBLID')
reporting_id = os.environ.get('MK6_REPORTING_TBLID')

inputFile = os.environ.get('MK6_WEEKLY_FILENAME')

''' Logging all of the variables '''
print("using dataset_id: " +str(dataset_id))
print("using project_id: "+str(project_id))
print("using working_id: "+str(working_id))
print("using weeklyWorking_id: "+str(weeklyWorking_id))
print("using weeklyInitial_id: "+str(weeklyInitial_id))
print("using reporting_id: "+str(reporting_id))
print("using inputFile: "+str(inputFile))

''' Getting Authenticated '''
print("Authenticating to Big Query")
client = bigquery.Client()
print("\n")

''' Loading Weekly File '''
print("About to load the Weekly file into Big Query")
loadId = utils.load_mkt6_csv(dataset_id, weeklyInitial_id, inputFile, client)
print('\n----------- LOADING JOB ID --------------\n')
print(loadId)
print('\n')

''' Creating weekly working table '''
print("About to create the working table from the initial load")
workingId = utils.create_working_table(project_id, dataset_id, weeklyInitial_id, weeklyWorking_id, client)
print('\n----------- WORKING JOB ID --------------\n')
print(workingId)
print('\n')

''' Merging base working with new working '''
print("About to merge the base working table to the new working table")
mergeId = utils.merge_working_tables(project_id, dataset_id, working_id, weeklyWorking_id, reporting_id, client)
print('\n----------- WORKING JOB ID --------------\n')
print(workingId)
print('\n')

''' Creating copy of the reporting table over the old working table '''
print("About to delete the base working table and copy the reporting table into its place")
copyId = utils.copy_table(project_id,dataset_id,reporting_id,working_id,client)
print('\n----------- COPY JOB ID --------------\n')
print(workingId)
print('\n')
