import os
from google.cloud import bigquery

dataset_id = 'color_orchids_external'
initialLoad_id = 'market6_initialLoad'
working_id = 'market6_working'
weeklyWorking_id = 'market6Weekly_working'
weeklyInitial_id = 'market6Weekly_initial'
project_id= 'bigquery-gitlabsync-co'

working_sql = '''
SELECT
CONCAT(Week_Name,"_",Manufacturer_Description,"_",Report_Short_Description,"_",CAST(RE_Store_Number as STRING),"_",Item_Description) as rowKey,
REGEXP_EXTRACT(Week_Name,'(^\\\\d{4})') as year,
REGEXP_EXTRACT(Week_Name,'^.*\\\\((\\\\d{2})\\\\)') as wkNum,
concat(REGEXP_EXTRACT(Week_Name,'(^\\\\d{4})'),REGEXP_EXTRACT(Week_Name,'^.*\\\\((\\\\d{2})\\\\)')) as yearWeek,
DATE_ADD(PARSE_DATE('%Y',REGEXP_EXTRACT(Week_Name,'(^\\\\d{4})')), INTERVAL (CAST (REGEXP_EXTRACT(Week_Name,'^.*\\\\((\\\\d{2})\\\\)') as INT64)) week) as weekDate,
t.Week_Name,
t.Item_Scan_Dept_Desc,
t.Item_Scan_Sub_Category_Desc,
t.Manufacturer_Code,
t.Manufacturer_Description,
t.Report_Short_Description,
t.RE_Store_Number,
t.Item_Description,
t.UPC,
t.Scanned_Retail_Dollars,
t.Scanned_Movement,
t.Scanned_LBS,
t.Gross_Margin_Dollars,
t.Store_Banner,
t.Store_Name,
t.Store_Address,
t.Store_Phone,
t.Store_City,
t.Store_County,
t.Store_State,
t.Store_Zip,
t.Store_Type,
PARSE_DATETIME('%m/%d/%Y %X %p',t.Store_Open_Dt) as Store_Open_Dt,
t.Store_Status,
PARSE_DATETIME('%m/%d/%Y %X %p',t.Store_Close_Dt) as Store_Close_Dt,
t.RE_Store_Zone_Cd,
t.Store_Fuel,
t.Item_Scan_Price_Group,
t.Item_Scan_Price_Group_Description
FROM
'''

copy_sql = "SELECT * FROM `{0}.{1}.{2}`"

merge_working_sql = '''
select
   wt.rowKey,
   wt.year,
   wt.wkNum,
   wt.yearWeek,
   wt.weekDate,
   wt.Week_Name,
   wt.Item_Scan_Dept_Desc,
   wt.Item_Scan_Sub_Category_Desc,
   wt.Manufacturer_Code,
   wt.Manufacturer_Description,
   wt.Report_Short_Description,
   wt.RE_Store_Number,
   wt.Item_Description,
   wt.UPC,
   wt.Scanned_Retail_Dollars,
   wt.Scanned_Movement,
   wt.Scanned_LBS,
   wt.Gross_Margin_Dollars,
   wt.Store_Banner,
   wt.Store_Name,
   wt.Store_Address,
   wt.Store_Phone,
   wt.Store_City,
   wt.Store_County,
   wt.Store_State,
   wt.Store_Zip,
   wt.Store_Type,
   wt.Store_Open_Dt,
   wt.Store_Status,
   wt.Store_Close_Dt,
   wt.RE_Store_Zone_Cd,
   wt.Store_Fuel,
   wt.Item_Scan_Price_Group,
   wt.Item_Scan_Price_Group_Description

from `{0}.{1}.{2}` as wt
   where wt.rowKey not in (select distinct(iwt.rowKey) from `{0}.{1}.{3}` as iwt)

UNION ALL

select
   nwt.rowKey,
   nwt.year,
   nwt.wkNum,
   nwt.yearWeek,
   nwt.weekDate,
   nwt.Week_Name,
   nwt.Item_Scan_Dept_Desc,
   nwt.Item_Scan_Sub_Category_Desc,
   nwt.Manufacturer_Code,
   nwt.Manufacturer_Description,
   nwt.Report_Short_Description,
   nwt.RE_Store_Number,
   nwt.Item_Description,
   nwt.UPC,
   nwt.Scanned_Retail_Dollars,
   nwt.Scanned_Movement,
   nwt.Scanned_LBS,
   nwt.Gross_Margin_Dollars,
   nwt.Store_Banner,
   nwt.Store_Name,
   nwt.Store_Address,
   nwt.Store_Phone,
   nwt.Store_City,
   nwt.Store_County,
   nwt.Store_State,
   nwt.Store_Zip,
   nwt.Store_Type,
   nwt.Store_Open_Dt,
   nwt.Store_Status,
   nwt.Store_Close_Dt,
   nwt.RE_Store_Zone_Cd,
   nwt.Store_Fuel,
   nwt.Item_Scan_Price_Group,
   nwt.Item_Scan_Price_Group_Description

from  `{0}.{1}.{3}` as nwt
'''

mkt6_schema = [
    bigquery.SchemaField('Week_Name', 'STRING'),
    bigquery.SchemaField('Item_Scan_Dept_Desc', 'STRING'),
    bigquery.SchemaField('Item_Scan_Category_Desc', 'STRING'),
    bigquery.SchemaField('Item_Scan_Sub_Category_Desc', 'STRING'),
    bigquery.SchemaField('Manufacturer_Code', 'STRING'),
    bigquery.SchemaField('Manufacturer_Description', 'STRING'),
    bigquery.SchemaField('Report_Short_Description', 'STRING'),
    bigquery.SchemaField('RE_Store_Number', 'FLOAT'),
    bigquery.SchemaField('Item_Description','STRING'),
    bigquery.SchemaField('UPC','STRING'),
    bigquery.SchemaField('Scanned_Retail_Dollars','FLOAT'),
    bigquery.SchemaField('Scanned_Movement','INTEGER'),
    bigquery.SchemaField('Scanned_LBS','FLOAT'),
    bigquery.SchemaField('Gross_Margin_Dollars','NUMERIC'),
    bigquery.SchemaField('Store_Banner','STRING'),
    bigquery.SchemaField('Store_Name','STRING'),
    bigquery.SchemaField('Store_Address','STRING'),
    bigquery.SchemaField('Store_Phone','STRING'),
    bigquery.SchemaField('Store_City','STRING'),
    bigquery.SchemaField('Store_County','STRING'),
    bigquery.SchemaField('Store_State','STRING'),
    bigquery.SchemaField('Store_Zip','STRING'),
    bigquery.SchemaField('Store_Type','STRING'),
    bigquery.SchemaField('Store_Open_Dt','STRING'),
    bigquery.SchemaField('Store_Status','STRING'),
    bigquery.SchemaField('Store_Close_Dt','STRING'),
    bigquery.SchemaField('RE_Store_Zone_Cd','STRING'),
    bigquery.SchemaField('Store_Fuel','STRING'),
    bigquery.SchemaField('Item_Scan_Price_Group','STRING'),
    bigquery.SchemaField('Item_Scan_Price_Group_Description','STRING')
]

def delete_destination(dsId,tblId, bqClt):
    '''
    Pass in the id of the dataset it (dsId) and the table id (tblId)
    Then use the bigquery client (bqClt) to delete the table
    '''
    table_ref = bqClt.dataset(dsId).table(tblId)
    bqClt.delete_table(table_ref)  # API request
    print('Table {}:{} deleted.'.format(dataset_id, tblId))

def create_working_table_sql(prjId,dsId, tblId):
    '''
    Using the stub SQL create the SQL that will create a working tableself.

    The things needed are the following:
    - project id: prjId
    - Dataset Id: dsId
    - table Id: tblId

    This SQL can then be used to create the working table
    '''
    return working_sql +' `{}.{}.{}` as t'.format(prjId,dsId,tblId)

def create_copy_sql(prjId, dsId, rptId):
    '''
    SQL to create a copy of the reporting table..

    parameters:
    - project id: prjId
    - dataset id: dsId
    - report id: rptId
    '''
    return copy_sql.format(prjId,dsId,rptId)

def create_working_merge_sql(prjId,dsId,srcWrkTbl, newWrkTbl):
    '''
    This will create the sql that will allow you to merge the new incoming file
      with the existing table.

    What this means is that base table is queried for all entries that are not in the new table.
    This is then combined with the new table by using a "UNION ALL"

    parameters:
    - project id: prjId
    - dataset id: dsId
    - source working table: srcWrkTbl (this is the base table... should be much bigger)
    - new working table: newWrkTbl (this is the incoming data to be merged)
    '''

    return merge_working_sql.format(prjId,dsId,srcWrkTbl,newWrkTbl)

def copy_table(prjId, dsId, srcTblId, cpyTblId, bqClt):
    '''
    Make a copy of table in BigQuery... for whatever reason you want!!

    Need the following:
    - Project ID: prjId
    - Dataset ID: dsId
    - Source Table ID: srcTblId
    - copy Table ID: cpyTblId
    - Bigquery Client: bqClt

    Here are the steps,
    1. Delete the copy table (if it doesn't exist, it will fail... but we catch and print that message)
    2. Create the Job Configuration
    3. Create the copy table sql
    4. Create the table copy
    5. Print completion stats
    '''
    try:
        delete_destination(dsId,cpyTblId, bqClt)
    except:
        print("Could not delete the copy table, perhaps it doesn't exist: {}".format(cpyTblId))


    job_config = bigquery.QueryJobConfig()
    # Set the destination table
    table_ref = bqClt.dataset(dsId).table(cpyTblId)
    job_config.destination = table_ref

    sql = create_copy_sql(prjId,dsId,srcTblId)

    # Start the query, passing in the extra configuration.
    query_job = bqClt.query(
        sql,
        # Location must match that of the dataset(s) referenced in the query
        # and of the destination table.
        location='US',
        job_config=job_config)  # API request - starts the query

    try:
        query_job.result()  # Waits for the query to finish
    except:
        print("there was a problem executing the query....")
        print("Job ID: "+str(query_job.job_id))
        print("Error Info: "+str(query_job.error_result))
        print("Errors: "+str(query_job.errors))

    print('Query Job Complete (Job ID: {}), Table Copied: (Loaded {} rows) Query results loaded to table {}'.format(query_job.job_id,query_job.num_dml_affected_rows,table_ref.path))
    return query_job.job_id

def create_working_table(prjId, dsId, srcTblId, dstTblId, bqClt):
    '''
    Given a source table (the incremental table usually), create the working table

    Need the following:
    - Project ID: prjId
    - Dataset ID: dsId
    - Source Table ID: srcTblId
    - Destination Table ID: dstTblId
    - Bigquery Client: bqClt

    Here are the steps,
    1. Delete the destination table (if it doesn't exist, it will fail... but we catch and print that message)
    2. Create the Job Configuration
    3. Create the working table sql
    4. Create the working table given the built SQL
    5. Print completion stats
    '''
    try:
        delete_destination(dsId,dstTblId, bqClt)
    except:
        print("Could not delete the destination table, perhaps it doesn't exist: {}".format(dstTblId))


    job_config = bigquery.QueryJobConfig()
    # Set the destination table
    table_ref = bqClt.dataset(dsId).table(dstTblId)
    job_config.destination = table_ref

    sql = create_working_table_sql(prjId,dsId,srcTblId)

    # Start the query, passing in the extra configuration.
    query_job = bqClt.query(
        sql,
        # Location must match that of the dataset(s) referenced in the query
        # and of the destination table.
        location='US',
        job_config=job_config)  # API request - starts the query

    try:
        query_job.result()  # Waits for the query to finish
    except:
        print("there was a problem executing the query....")
        print("Job ID: "+str(query_job.job_id))
        print("Error Info: "+str(query_job.error_result))
        print("Errors: "+str(query_job.errors))

    print('Query Job Complete (Job ID: {}), Working Table Created: (Loaded {} rows) Query results loaded to table {}'.format(query_job.job_id,query_job.num_dml_affected_rows,table_ref.path))
    return query_job.job_id

def merge_working_tables(prjId, dsId, wrkTblId, newWrkTblId, rptTblId, bqClt):
    '''
    Given a source table (the incremental table usually), create the working table

    Need the following:
    - Project ID: prjId
    - Dataset ID: dsId
    - Working Table ID: srcTblId  (This is the base table that has a large number of rows)
    - New Working Table ID: dstTblId  (This is the weekly update rows)
    - Report Table ID: rptTblId  (This is the table that all reporting will be done off of)
    - Bigquery Client: bqClt

    Here are the steps,
    1. Delete the reporting table
    2. Create the Job Configuration
    3. Create the merge table sql
    4. Create the report table given the built SQL
    5. Print completion stats
    '''
    try:
        delete_destination(dsId,rptTblId, bqClt)
    except:
        print("Could not delete the reporting table, perhaps it doesn't exist: {}".format(rptTblId))


    job_config = bigquery.QueryJobConfig()
    # Set the destination table
    table_ref = bqClt.dataset(dsId).table(rptTblId)  # The destination table is the reporting table
    job_config.destination = table_ref

    sql = create_working_merge_sql(prjId, dsId, wrkTblId, newWrkTblId)

    # Start the query, passing in the extra configuration.
    query_job = bqClt.query(
        sql,
        # Location must match that of the dataset(s) referenced in the query
        # and of the destination table.
        location='US',
        job_config=job_config)  # API request - starts the query

    try:
        query_job.result()  # Waits for the query to finish
    except:
        print("there was a problem executing the query....")
        print("Job ID: "+str(query_job.job_id))
        print("Error Info: "+str(query_job.error_result))
        print("Errors: "+str(query_job.errors))

    print('Query Job Complete (Job ID: {}), Merged Reporting Table Created: (Loaded {} rows) Query results loaded to table {}'.format(query_job.job_id,query_job.num_dml_affected_rows,table_ref.path))
    return query_job.job_id

def load_mkt6_csv(dsId, dstTblid, filename, bqClt, skip_rows=1,schema=mkt6_schema):
    '''
    Load a file into an "ingestion" table.  From there the rest of the process kicks off.

    The following is expected:
    - Dataset ID: dsId
    - Destination Table ID: dstTblId
    - File Name: filename
    - Bigquery Client: bqClt
    - Skip Rows:  skip_rows  (Default is 1, if anything else send it!!!)
    - Schema for Table: schema (Default is to use the Market 6 Sherlock Matrix format)

    Steps:
    1) Delete the destination table (if it doesn't exist, it will fail and a message will be printed)
    2) Setup the Job configuration
    3) Open the file
    4) Load the file into BigQuery using the passed in Schema
    5) Print job stats

    Done... yeah buddy!
    '''
    try:
        delete_destination(dsId,dstTblid, bqClt)
    except:
        print("Could not delete the destination table, perhaps it doesn't exist: {}".format(dstTblid))

    dataset_ref = bqClt.dataset(dsId)
    table_ref = dataset_ref.table(dstTblid)
    job_config = bigquery.LoadJobConfig()
    job_config.source_format = bigquery.SourceFormat.CSV
    job_config.skip_leading_rows = skip_rows
    #job_config.autodetect = True
    job_config.schema = schema

    with open(filename, 'rb') as source_file:
        job = bqClt.load_table_from_file(
            source_file,
            table_ref,
            location='US',  # Must match the destination dataset location.
            job_config=job_config)  # API request

    try:
        job.result()  # Waits for table load to complete.
    except:
        print(" The job failed... ")
        print("Job ID: "+str(job.job_id))
        print("Error Info: "+str(job.error_result))
        print("Errors: "+str(job.errors))

    print('Job Finished: Job ID: {}, Loaded {} rows into {}:{}.'.format(job.job_id,job.output_rows, dsId, dstTblid))
    return job.job_id
